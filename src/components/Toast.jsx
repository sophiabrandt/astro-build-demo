import React from 'react';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const Toast = () => {
    const notify = () => toast("Astro is awesome!");

    return (
      <div>
		  <button style={{marginTop: "2em", display: "block"}} onClick={notify}>Click Me</button>
        <ToastContainer />
      </div>
    );
  }
